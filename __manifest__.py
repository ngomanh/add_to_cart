# -*- coding: utf-8 -*-
{
    'name': "s_shopify_sticky_add_to_cart",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 's_base', 'mail', 's_shopify_data', 'website'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'data/app_data.xml',
        'views/s_shopify_sticky_bar.xml',
        'views/s_shopify_sticky_cart.xml',
        'views/s_shopify_mini_cart.xml',
        'views/assets_backend_inherit_view.xml',
        'views/s_shopify_analytics.xml',
        'views/onboarding_s_shopify_sticky_add_to_card_analytics.xml',
        'views/shopify_integration_bought_together.xml'
    ],
}

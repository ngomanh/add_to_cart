# -*- coding: utf-8 -*-
import base64
import json
import logging
import os
import random
import string
import traceback

import shopify
import werkzeug

from odoo import http
from odoo.http import request
from ...s_base.controllers.sp_controllers import SpController

_logger = logging.getLogger(__name__)


class SpDataController(SpController):

    def shopify_auth_s_shopify_sticky_add_to_cart(self, **kw):
        # check neccessary param
        # todo validate hmac
        # todo validate shop_url
        if 'shop' in kw:
            # generate permission url
            current_app = request.env.ref('s_shopify_sticky_add_to_cart.s_shopify_sticky_add_to_cart_app').sudo()
            if current_app:
                shopify.ShopifyResource.clear_session()
                shopify.Session.setup(
                    api_key=current_app.sp_api_key,
                    secret=current_app.sp_api_secret_key)
                shopify_session = shopify.Session(kw['shop'], current_app.sp_api_version)
                scope = [
                    "read_products",
                    "read_product_listings",
                    "write_script_tags",
                ]
                redirect_uri = current_app.base_url + "/shopify/finalize/s_shopify_sticky_add_to_cart"
                permission_url = shopify_session.create_permission_url(
                    scope, redirect_uri)
                return werkzeug.utils.redirect(permission_url)

        return "Hello, world"

    def shopify_finalize_s_shopify_sticky_add_to_cart(self, **kw):
        try:
            # check neccessary param
            # todo validate hmac
            # todo validate shop_url
            if 'shop' in kw:
                current_app = request.env.ref('s_shopify_sticky_add_to_cart.s_shopify_sticky_add_to_cart_app').sudo()
                if current_app:
                    shopify.Session.setup(
                        api_key=current_app.sp_api_key,
                        secret=current_app.sp_api_secret_key)
                    shopify_session = shopify.Session(kw['shop'], current_app.sp_api_version)
                    token = shopify_session.request_token(kw)
                    shopify.ShopifyResource.activate_session(shopify_session)
                    script = shopify.ScriptTag(
                        dict(event='onload',
                             src=current_app.webhook_base_url + "/s_shopify_sticky_add_to_cart/static/src/js/sticky_add_to_cart.js")).save()

                    if token:
                        # todo check active, unactive shop
                        current_s_sp_shop = request.env['s.sp.shop'].sudo().search([('base_url', '=', kw['shop'])],
                                                                                   limit=1)
                        if not current_s_sp_shop:
                            client = shopify.GraphQL()
                            query = '''
                                                      {
                                                        shop {
                                                          id
                                                          name
                                                          email
                                                          myshopifyDomain
                                                          contactEmail
                                                          currencyCode
                                                        }
                                                      }
                                                  '''
                            result = client.execute(query)
                            current_shop_dict = json.loads(result)
                            # check and create s_sp_shop, res_user, res_company

                            # create company
                            current_company = http.request.env['res.company'].sudo().search([('name', '=', kw['shop'])],
                                                                                            limit=1)
                            if not current_company:
                                with open(os.path.abspath(
                                        os.path.dirname(os.path.dirname(__file__))) + '/static/src/img/favicon.png',
                                          "rb") as image_file:
                                    encoded_allfetch_image = base64.b64encode(image_file.read())
                                current_company = http.request.env['res.company'].sudo().create({
                                    'logo': False, 'currency_id': 2, 'sequence': 10,
                                    'favicon': encoded_allfetch_image,
                                    'name': kw['shop'], 'street': False, 'street2': False, 'city': False,
                                    'state_id': False,
                                    'zip': False, 'country_id': False, 'phone': False, 'email': False, 'website': False,
                                    'vat': False, 'company_registry': False, 'parent_id': False
                                })
                            # create user

                            # generate password
                            letters = string.ascii_lowercase
                            password_generate = ''.join(random.choice(letters) for i in range(30))
                            current_s_sp_shop = request.env['s.sp.shop'].sudo().create({
                                'name': current_shop_dict['data']['shop']['name'],
                                'base_url': kw['shop'],
                                'email': current_shop_dict['data']['shop']['email'],
                                'currency_code': current_shop_dict['data']['shop']['currencyCode'],
                                'currency_id': 2,
                                'password': password_generate,
                            })

                            current_user = http.request.env['res.users'].sudo().search([('login', '=', kw['shop'])],
                                                                                       limit=1)
                            if not current_user:
                                current_user = http.request.env['res.users'].sudo().create({
                                    'company_ids': [[6, False, [current_company.id]]], 'company_id': current_company.id,
                                    'active': True,
                                    'lang': 'en_US', 'tz': 'Europe/Brussels',
                                    'image_1920': False, '__last_update': False,
                                    'name': kw['shop'], 'email': current_shop_dict['data']['shop']['email'],
                                    'login': kw['shop'],
                                    'password': password_generate,
                                    'is_client': True,
                                    'action_id': False,
                                    'sp_shop_id': current_s_sp_shop.id
                                })

                        else:
                            # todo active shop
                            current_s_sp_shop.status = True

                        # check and create s_sp_app
                        current_s_sp_app = http.request.env['s.sp.app'].sudo().search(
                            [('sp_shop_id', '=', current_s_sp_shop.id), ('s_app_id', '=', current_app.id)], limit=1)
                        if not current_s_sp_app:
                            # todo update shop plan
                            current_s_sp_app = http.request.env['s.sp.app'].sudo().create({
                                'sp_shop_id': current_s_sp_shop.id,
                                's_app_id': current_app.id,
                                'status': True,
                                'token': token,
                                's_plan_id': None
                            })
                        else:
                            current_s_sp_app.status = True
                            current_s_sp_app.token = token
                        # check and create webhook
                        # todo check and create webhook

                        existing_webhooks = shopify.Webhook.find()
                        if not existing_webhooks or current_s_sp_app.is_force_update_webhook:
                            # delete all existing webhook
                            for webhook in existing_webhooks:
                                if webhook.id and 's_shopify_sticky_add_to_cart/webhook' in webhook.attributes[
                                    'address']:
                                    shopify.Webhook.find(webhook.id).destroy()
                            current_s_sp_app.is_force_update_webhook = False
                        # save web hook data

                        existing_webhooks = shopify.Webhook.find()
                        if existing_webhooks:
                            existing_webhooks_list = []
                            for webhook in existing_webhooks:
                                if type(webhook) is not dict:
                                    webhook = webhook.to_dict()
                                existing_webhooks_list.append(webhook)
                            current_s_sp_app.webhook_data = json.dumps(existing_webhooks_list)
                        # check if is_first_product_data_action is False then action add queue
                        current_shop_global_setting = http.request.env['s.shopify.data.global.setting'].sudo().search(
                            [('sp_shop_id', '=', current_s_sp_shop.id), ('sp_app_id', '=', current_s_sp_app.id)],
                            limit=1)
                        if not current_shop_global_setting:
                            current_shop_global_setting = http.request.env[
                                's.shopify.data.global.setting'].sudo().create({
                                'sp_shop_id': current_s_sp_shop.id,
                                'sp_app_id': current_s_sp_app.id,
                            })
                        # if not current_shop_global_setting.is_first_product_data_action:
                        #     print('shopify_fetch_all_products')
                        #     current_s_sp_app.sudo().shopify_fetch_all_products()
                        #     current_shop_global_setting.is_first_product_data_action = True
                        # action auto login
                        db = http.request.env.cr.dbname
                        request.env.cr.commit()
                        uid = request.session.authenticate(db, kw['shop'], current_s_sp_shop.password)
        except Exception as ex:
            _logger.error(traceback.format_exc())
            return werkzeug.utils.redirect('https://google.com/')

        return werkzeug.utils.redirect('https://shopify.com/')

    @http.route('/s_shopify_sticky_add_to_cart/config_sticky_cart', auth='public', type='json', cors='*',
                csrf=False)
    def fetch_config_sticky_cart(self, **kw):
        dict_config = {}
        config = http.request.env['s.shopify.sticky.cart.config'].sudo().search([], limit=1)
        if config:
            dict_config['is_enable_for_desktop'] = config.is_enable_for_desktop
            dict_config['is_enable_for_mobile'] = config.is_enable_for_mobile
            dict_config['position'] = config.position
            dict_config['button_action'] = config.button_action
            dict_config['icon_background_color'] = config.icon_background_color
            dict_config['icon_color'] = config.icon_color
            dict_config['counter_background_color'] = config.counter_background_color
            dict_config['counter_number_color'] = config.counter_number_color
        return dict_config

    @http.route('/s_shopify_sticky_add_to_cart/config_mini_cart', auth='public', type='json', cors='*',
                csrf=False)
    def fetch_config_mini_cart(self, **kw):
        dict_config = {}
        config = http.request.env['s.shopify.mini.cart.config'].sudo().search([], limit=1)
        if config:
            dict_config['is_enable_for_desktop'] = config.is_enable_for_desktop
            dict_config['is_enable_for_mobile'] = config.is_enable_for_mobile
            dict_config['type'] = config.type
            dict_config['checkout_button_text'] = config.checkout_button_text
            dict_config['checkout_button_color'] = config.checkout_button_color
            dict_config['checkout_text_color'] = config.checkout_text_color
            dict_config['buy_now_button_text'] = config.buy_now_button_text
            dict_config['continue_background_color'] = config.continue_background_color
            dict_config['continue_text_color'] = config.continue_text_color
        return dict_config

    @http.route('/s_shopify_sticky_add_to_cart/config_sticky_bar', auth='public', type='json', cors='*',
                csrf=False)
    def fetch_config_sticky_bar(self, **kw):
        dict_config = {}
        config = http.request.env['s.shopify.sticky.bar.config'].sudo().search([], limit=1)
        if config:
            dict_config['is_enable_for_desktop'] = config.is_enable_for_desktop
            dict_config['is_enable_for_mobile'] = config.is_enable_for_mobile
            dict_config['position'] = config.position
            dict_config['add_to_cart_label'] = config.add_to_cart_label
            dict_config['buy_now_label'] = config.buy_now_label
            dict_config['out_of_stock_label'] = config.out_of_stock_label
            dict_config['background_color'] = config.background_color
            dict_config['add_to_cart_button_color'] = config.add_to_cart_button_color
            dict_config['add_to_cart_border_color'] = config.add_to_cart_border_color
            dict_config['add_to_cart_text_color'] = config.add_to_cart_text_color
            dict_config['buy_now_button_color'] = config.buy_now_button_color
            dict_config['buy_now_border_color'] = config.buy_now_border_color
            dict_config['buy_now_text_color'] = config.buy_now_text_color
        return dict_config

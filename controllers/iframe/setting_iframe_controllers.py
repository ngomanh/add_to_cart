import logging

import cssutils

from odoo import http
from odoo.http import request

cssutils.log.setLevel(logging.CRITICAL)

_logger = logging.getLogger(__name__)


class SettingIframeSticky(http.Controller):

    @http.route('/iframe/s_shopify_sticky_add_to_cart/setting', type='http', auth="public")
    def iframe_s_shopify_sticky_add_to_cart_setting(self):
        response = request.render('s_shopify_sticky_add_to_cart.onboarding_s_shopify_sticky_add_to_setting_onboarding_iframe_template')
        response.headers['X-Frame-Options'] = 'SAMEORIGIN'
        return response



# -*- coding: utf-8 -*-
import json
import logging
import random
import string
import traceback
import base64
import shopify
import werkzeug
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

from odoo import http
from odoo.http import request
from ...s_base.controllers.sp_controllers import SpController
import os

_logger = logging.getLogger(__name__)


class SpStickyCartController(SpController):
    def shopify_auth_s_shopify_sticky_add_to_cart(self, **kw):
        # check neccessary param
        # todo validate hmac
        # todo validate shop_url
        if 'shop' in kw:
            # generate permission url
            current_app = request.env.ref('s_shopify_sticky_add_to_cart.s_shopify_sticky_add_to_cart_app').sudo()
            if current_app:
                shopify.ShopifyResource.clear_session()
                shopify.Session.setup(
                    api_key=current_app.sp_api_key,
                    secret=current_app.sp_api_secret_key)
                shopify_session = shopify.Session(kw['shop'], current_app.sp_api_version)
                scope = [
                    "read_orders",
                    "read_themes",
                    "write_themes",
                    "read_script_tags",
                    "write_script_tags"
                ]
                redirect_uri = current_app.base_url + "/shopify/finalize/s_shopify_sticky_add_to_cart"
                permission_url = shopify_session.create_permission_url(
                    scope, redirect_uri)
                return werkzeug.utils.redirect(permission_url)

        return "Hello, world, TikTok"

    def shopify_finalize_s_shopify_sticky_add_to_cart(self, **kw):
        try:
            # check neccessary param
            # todo validate hmac
            # todo validate shop_url
            if 'shop' in kw:
                current_app = request.env.ref('s_shopify_sticky_add_to_cart.s_shopify_sticky_add_to_cart_app').sudo()
                if current_app:
                    shopify.Session.setup(
                        api_key=current_app.sp_api_key,
                        secret=current_app.sp_api_secret_key)
                    shopify_session = shopify.Session(kw['shop'], current_app.sp_api_version)
                    token = shopify_session.request_token(kw)
                    shopify.ShopifyResource.activate_session(shopify_session)
                    script = shopify.ScriptTag(
                        dict(event='onload',
                             src=current_app.webhook_base_url + "/s_shopify_sticky_add_to_cart/static/src/js/sticky_add_to_cart.js")).save()

                    if token:
                        # todo check active, unactive shop
                        current_s_sp_shop = request.env['s.sp.shop'].sudo().search([('base_url', '=', kw['shop'])], limit=1)
                        if not current_s_sp_shop:
                            client = shopify.GraphQL()
                            query = '''
                                                      {
                                                        shop {
                                                          id
                                                          name
                                                          email
                                                          myshopifyDomain
                                                          contactEmail
                                                          currencyCode
                                                        }
                                                      }
                                                  '''
                            result = client.execute(query)
                            current_shop_dict = json.loads(result)
                            # check and create s_sp_shop, res_user, res_company
                            # create company
                            current_company = http.request.env['res.company'].sudo().search([('name', '=', kw['shop'])], limit=1)
                            if not current_company:
                                with open(os.path.abspath(os.path.dirname(os.path.dirname(__file__))) + '/static/src/img/favicon.png', "rb") as image_file:
                                    encoded_allfetch_image = base64.b64encode(image_file.read())
                                current_company = http.request.env['res.company'].sudo().create({
                                    'logo': False, 'currency_id': 2, 'sequence': 10,
                                    'favicon': encoded_allfetch_image,
                                    'name': kw['shop'], 'street': False, 'street2': False, 'city': False, 'state_id': False,
                                    'zip': False, 'country_id': False, 'phone': False, 'email': False, 'website': False,
                                    'vat': False, 'company_registry': False, 'parent_id': False
                                })
                            # create user
                            # generate password
                            letters = string.ascii_lowercase
                            password_generate = ''.join(random.choice(letters) for i in range(30))
                            current_s_sp_shop = request.env['s.sp.shop'].sudo().create({
                                'name': current_shop_dict['data']['shop']['name'],
                                'base_url': kw['shop'],
                                'email': current_shop_dict['data']['shop']['email'],
                                'currency_code': current_shop_dict['data']['shop']['currencyCode'],
                                'currency_id': 2,
                                'password': password_generate,
                            })
                            current_user = http.request.env['res.users'].sudo().search([('login', '=', kw['shop'])], limit=1)
                            if not current_user:
                                current_user = http.request.env['res.users'].sudo().create({
                                    'company_ids': [[6, False, [current_company.id]]], 'company_id': current_company.id,
                                    'active': True,
                                    'lang': 'en_US', 'tz': 'Europe/Brussels',
                                    'image_1920': False, '__last_update': False,
                                    'name': kw['shop'], 'email': current_shop_dict['data']['shop']['email'], 'login': kw['shop'],
                                    'password': password_generate,
                                    'is_client': True,
                                    'action_id': False,
                                    'sp_shop_id': current_s_sp_shop.id
                                })
                        else:
                            # todo active shop
                            current_s_sp_shop.status = True
                            current_user = http.request.env['res.users'].sudo().search([('login', '=', kw['shop'])],
                                                                                       limit=1)
                        # check and create s_sp_app
                        current_s_sp_app = http.request.env['s.sp.app'].sudo().search([('sp_shop_id', '=', current_s_sp_shop.id), ('s_app_id', '=', current_app.id)], limit=1)
                        if not current_s_sp_app:
                            # todo update shop plan
                            current_s_sp_app = http.request.env['s.sp.app'].sudo().create({
                                'sp_shop_id': current_s_sp_shop.id,
                                's_app_id': current_app.id,
                                'status': True,
                                'token': token,
                                's_plan_id': None
                            })
                        else:
                            current_s_sp_app.status = True
                            current_s_sp_app.install_status = True
                            current_s_sp_app.token = token
                        # check and create webhook
                        # todo check and create webhook
                        # pass
                        # action auto login
                        db = http.request.env.cr.dbname
                        request.env.cr.commit()
                        request.session.logout(keep_db=True)
                        uid = request.session.authenticate(db, kw['shop'], current_s_sp_shop.password)
                        sticky_cart_menu = request.env.ref('s_shopify_sticky_add_to_cart.menu_shopify_sticky_add_to_cart_list').id
                        sticky_cart_action = request.env.ref(
                            's_shopify_sticky_add_to_cart.s_shopify_sticky_add_to_cart_setting_action_window_id_force_hide_filter_group_by').id
                        redirectUrl = current_app.base_url + '/web?#menu_id=' + str(
                            sticky_cart_menu) + '&action_id=' + str(sticky_cart_action)
                        return werkzeug.utils.redirect(redirectUrl)

        except Exception as ex:
            _logger.error(traceback.format_exc())
            return werkzeug.utils.redirect('https://google.com/')

        return werkzeug.utils.redirect('https://google.com/')



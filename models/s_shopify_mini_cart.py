from odoo import models, fields


class MiniCartConfig(models.Model):
    _name = 's.shopify.mini.cart.config'

    is_enable_for_desktop = fields.Boolean(string="Enable for desktop")
    is_enable_for_mobile = fields.Boolean(string="Enable for mobile")
    type = fields.Selection(
        [('modal', 'Modal'),
         ('off_canvas', 'Off Canvas')],
        default='modal', string='Mini cart types')
    checkout_button_text = fields.Char(string="Checkout Button Text")
    checkout_button_color = fields.Char(string="Checkout Button Color")
    checkout_text_color = fields.Char(string="Checkout Text Color")
    buy_now_button_text = fields.Char(string="Buy Now Button Text")
    continue_background_color = fields.Char(string="Continue Background Color")
    continue_text_color = fields.Char(string="Continue Text Color")

    def _open_form_config_shopify_mini_cart(self):
        config_exist = self.env['s.shopify.mini.cart.config'].sudo().search([], limit=1)
        view_id = self.env.ref('s_shopify_sticky_add_to_cart.shopify_mini_cart_view_form').id
        if config_exist:
            return {
                'name': 'Mini Cart Config',
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'target': 'current',
                'views': [(view_id, 'form')],
                'res_id': config_exist.id,
                'res_model': 's.shopify.mini.cart.config',
            }
        else:
            return {
                'name': 'Mini Cart Config',
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'target': 'current',
                'views': [(view_id, 'form')],
                'res_model': 's.shopify.mini.cart.config',
            }

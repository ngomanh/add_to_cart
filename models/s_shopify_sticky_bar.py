from odoo import models, fields


class StickyBarConfig(models.Model):
    _name = 's.shopify.sticky.bar.config'

    is_enable_for_desktop = fields.Boolean(string="Enable for desktop")
    is_enable_for_mobile = fields.Boolean(string="Enable for mobile")
    position = fields.Selection(
        [('top', 'Top'),
         ('bottom', 'Bottom')],
        default='bottom', string='Position')
    add_to_cart_label = fields.Char(string="Add to cart button text")
    buy_now_label = fields.Char(string="Buy now button text")
    out_of_stock_label = fields.Char(string="Out of stock button text")
    background_color = fields.Char(string="Background Color")

    add_to_cart_button_color = fields.Char(string="Add to cart button color")
    add_to_cart_border_color = fields.Char(string="Add to cart border color")
    add_to_cart_text_color = fields.Char(string="Add to cart text color")

    buy_now_button_color = fields.Char(string="Buy now button color")
    buy_now_border_color = fields.Char(string="Buy now border color")
    buy_now_text_color = fields.Char(string="Buy now text color")

    def _open_form_config_shopify_sticky_bar(self):
        config_exist = self.env['s.shopify.sticky.bar.config'].sudo().search([], limit=1)
        view_id = self.env.ref('s_shopify_sticky_add_to_cart.shopify_sticky_bar_view_form').id
        if config_exist:
            return {
                'name': 'Sticky Bar Config',
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'target': 'current',
                'views': [(view_id, 'form')],
                'res_id': config_exist.id,
                'res_model': 's.shopify.sticky.bar.config',
            }
        else:
            return {
                'name': 'Sticky Bar Config',
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'target': 'current',
                'views': [(view_id, 'form')],
                'res_model': 's.shopify.sticky.bar.config',
            }
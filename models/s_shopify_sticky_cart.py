from odoo import models, fields


class StickyCartConfig(models.Model):
    _name = 's.shopify.sticky.cart.config'

    is_enable_for_desktop = fields.Boolean(string="Enable for desktop")
    is_enable_for_mobile = fields.Boolean(string="Enable for mobile")
    position = fields.Selection(
        [('top_left', 'Top Left'),
         ('top_right', 'Top Right'),
         ('middle_left', 'Middle Left'),
         ('middle_right', 'Middle Right'),
         ('bottom_left', 'Bottom Left'),
         ('bottom_left', 'Bottom Right')],
        default='top_left', string='Position')
    button_action = fields.Selection(
        [('go_to_cart', 'Go to cart page'),
         ('go_to_checkout', 'Go to checkout page')],
        default='go_to_cart', string='Button Action')

    sticky_cart_icon = fields.Char(string="Icon")
    icon_background_color = fields.Char(string="Icon Background Color")
    icon_color = fields.Char(string="Icon Color")
    counter_background_color = fields.Char(string="Counter Background Color")
    counter_number_color = fields.Char(string="Counter Number Color")

    def _open_form_config_shopify_sticky_cart(self):
        cart_config_exist = self.env['s.shopify.sticky.cart.config'].sudo().search([], limit=1)
        view_id = self.env.ref('s_shopify_sticky_add_to_cart.shopify_sticky_cart_view_form').id
        if cart_config_exist:
            return {
                'name': 'Sticky Cart Config',
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'target': 'current',
                'views': [(view_id, 'form')],
                'res_id': cart_config_exist.id,
                'res_model': 's.shopify.sticky.cart.config',
            }
        else:
            return {
                'name': 'Sticky Cart Config',
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'target': 'current',
                'views': [(view_id, 'form')],
                'res_model': 's.shopify.sticky.cart.config',
            }

    def set_icon_sticky_cart(self, **kwargs):
        if 'icon' in kwargs:
            self.sticky_cart_icon = kwargs['icon']
            print(self.sticky_cart_icon)
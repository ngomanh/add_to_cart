import logging
from odoo import models, fields, api
from odoo.http import request
import requests
import json
import shopify
import os
from ..controllers import sp_sticky_add_to_cart_controllers


_logger = logging.getLogger(__name__)


class ShopifyStickyCartAnalytics(models.Model):
    _name = 's.sticky.cart.analytics'

    date_time = fields.Date(readonly=True)
    checkout_click = fields.Char(default='0', readonly=True)
    add_to_cart_click = fields.Char(default='0', readonly=True)
    shop_id = fields.Many2one('s.sp.shop', readonly=True, invisible=True)
    product_analytics = fields.One2many('s.sticky.product.analytics', 'sticky_cart')


class ShopifyProductAnalytics(models.Model):
    _name = 's.sticky.product.analytics'

    id_product = fields.Char(readonly=True)
    product_name = fields.Char(readonly=True)
    product_price = fields.Char(readonly=True)
    add_to_cart_click = fields.Integer(readonly=True)
    buy_now_click = fields.Integer(readonly=True)
    total_click = fields.Integer(readonly=True)
    shop_id = fields.Many2one('s.sp.shop')
    sticky_cart = fields.Many2one('s.sticky.cart.analytics')
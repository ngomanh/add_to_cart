odoo.define('s_shopify_bought_together.s_shopify_sticky_add_to_cart_setting_onboarding_form', function (require) {
    "use strict";
    var FormController = require('web.FormController');
    var FormView = require('web.FormView');
    var FormRenderer = require('web.FormRenderer');
    var viewRegistry = require('web.view_registry');
    var first_time_draw_iframe_interval;

    function update_generate_iframe_content(data) {
        if ($('#s_shopify_sticky_add_to_cart_setting_onboarding_iframe') != undefined) {
            var preview_html = '<div class="s_related_products_mgn">\n' +
                '    <div class="s_title">var_s_1_title</div>\n' +
                '    <div class="s_description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt\n' +
                '        ut labore et dolore magna aliqua. Bibendum est ultricies integer quis.\n' +
                '    </div>\n' +
                '    <div id="s_shopify_bought_together_setting_onboarding_slick" class="s_product_list">\n' +
                '        <div class="s_product_item">\n' +
                '            <div class="s_product_image">\n' +
                '                <img class="product-single__thumbnail-image"\n' +
                '                     src="//cdn.shopify.com/s/files/1/0405/1930/3334/products/image_png_758085347_110x110@2x.png?v=1602035422"\n' +
                '                     alt="Load image into Gallery viewer, Air Max 1 - luuthuy205">\n' +
                '            </div>\n' +
                '            <div class="s_product_detail">\n' +
                '                <div class="s_product_name">\n' +
                '                    DualShock 4 Wireless Controller for PlayStation 4 - Magma Red\n' +
                '                </div>\n' +
                '                <div class="s_product_options">\n' +
                '                    <label for="s_size">Size</label>\n' +
                '                    <select name="s_size" id="s_size">\n' +
                '                        <option value="M">M</option>\n' +
                '                        <option value="S">S</option>\n' +
                '                        <option value="L">L</option>\n' +
                '                        <option value="XL">XL</option>\n' +
                '                    </select>\n' +
                '                    <label for="s_color">Color</label>\n' +
                '                    <select name="s_color" id="s_color">\n' +
                '                        <option value="red">Red</option>\n' +
                '                        <option value="blue">Blue</option>\n' +
                '                        <option value="black">Black</option>\n' +
                '                        <option value="green">Green</option>\n' +
                '                    </select>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '            <div class="s_product_price">\n' +
                '                <span class="s_regular_price">250000&#8363;</span>\n' +
                '                <span class="s_old_price">300000&#8363;</span>\n' +
                '            </div>\n' +
                '            <div class="s_qty_product_counter">\n' +
                '                <span class="s_qty_minus s_qty_btn"></span>\n' +
                '                <span class="s_qty_value">125</span>\n' +
                '                <span class="s_qty_plus s_qty_btn"></span>\n' +
                '            </div>\n' +
                '            <button class="s_add_to_cart">Add to cart</button>\n' +
                '        </div>\n' +
                '        <div class="s_product_item">\n' +
                '            <div class="s_product_image">\n' +
                '                <img class="product-single__thumbnail-image"\n' +
                '                     src="//cdn.shopify.com/s/files/1/0405/1930/3334/products/image_png_758085347_110x110@2x.png?v=1602035422"\n' +
                '                     alt="Load image into Gallery viewer, Air Max 1 - luuthuy205">\n' +
                '            </div>\n' +
                '            <div class="s_product_detail">\n' +
                '                <div class="s_product_name">\n' +
                '                    DualShock 4 Wireless Controller for PlayStation 4 - Magma Red\n' +
                '                </div>\n' +
                '                <div class="s_product_options">\n' +
                '                    <label for="s_size">Size</label>\n' +
                '                    <select name="s_size" id="s_size">\n' +
                '                        <option value="M">M</option>\n' +
                '                        <option value="S">S</option>\n' +
                '                        <option value="L">L</option>\n' +
                '                        <option value="XL">XL</option>\n' +
                '                    </select>\n' +
                '                    <label for="s_color">Color</label>\n' +
                '                    <select name="s_color" id="s_color">\n' +
                '                        <option value="red">Red</option>\n' +
                '                        <option value="blue">Blue</option>\n' +
                '                        <option value="black">Black</option>\n' +
                '                        <option value="green">Green</option>\n' +
                '                    </select>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '            <div class="s_product_price">\n' +
                '                <span class="s_regular_price">250000&#8363;</span>\n' +
                '                <span class="s_old_price">300000&#8363;</span>\n' +
                '            </div>\n' +
                '            <div class="s_qty_product_counter">\n' +
                '                <span class="s_qty_minus s_qty_btn"></span>\n' +
                '                <span class="s_qty_value">125</span>\n' +
                '                <span class="s_qty_plus s_qty_btn"></span>\n' +
                '            </div>\n' +
                '            <button class="s_add_to_cart">Add to cart</button>\n' +
                '        </div>\n' +
                '        <div class="s_product_item">\n' +
                '            <div class="s_product_image">\n' +
                '                <img class="product-single__thumbnail-image"\n' +
                '                     src="//cdn.shopify.com/s/files/1/0405/1930/3334/products/image_png_758085347_110x110@2x.png?v=1602035422"\n' +
                '                     alt="Load image into Gallery viewer, Air Max 1 - luuthuy205">\n' +
                '            </div>\n' +
                '            <div class="s_product_detail">\n' +
                '                <div class="s_product_name">\n' +
                '                    DualShock 4 Wireless Controller for PlayStation 4 - Magma Red\n' +
                '                </div>\n' +
                '                <div class="s_product_options">\n' +
                '                    <label for="s_size">Size</label>\n' +
                '                    <select name="s_size" id="s_size">\n' +
                '                        <option value="M">M</option>\n' +
                '                        <option value="S">S</option>\n' +
                '                        <option value="L">L</option>\n' +
                '                        <option value="XL">XL</option>\n' +
                '                    </select>\n' +
                '                    <label for="s_color">Color</label>\n' +
                '                    <select name="s_color" id="s_color">\n' +
                '                        <option value="red">Red</option>\n' +
                '                        <option value="blue">Blue</option>\n' +
                '                        <option value="black">Black</option>\n' +
                '                        <option value="green">Green</option>\n' +
                '                    </select>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '            <div class="s_product_price">\n' +
                '                <span class="s_regular_price">250000&#8363;</span>\n' +
                '                <span class="s_old_price">300000&#8363;</span>\n' +
                '            </div>\n' +
                '            <div class="s_qty_product_counter">\n' +
                '                <span class="s_qty_minus s_qty_btn"></span>\n' +
                '                <span class="s_qty_value">125</span>\n' +
                '                <span class="s_qty_plus s_qty_btn"></span>\n' +
                '            </div>\n' +
                '            <button class="s_add_to_cart">Add to cart</button>\n' +
                '        </div>\n' +
                '        <div class="s_product_item">\n' +
                '            <div class="s_product_image">\n' +
                '                <img class="product-single__thumbnail-image"\n' +
                '                     src="//cdn.shopify.com/s/files/1/0405/1930/3334/products/image_png_758085347_110x110@2x.png?v=1602035422"\n' +
                '                     alt="Load image into Gallery viewer, Air Max 1 - luuthuy205">\n' +
                '            </div>\n' +
                '            <div class="s_product_detail">\n' +
                '                <div class="s_product_name">\n' +
                '                    DualShock 4 Wireless Controller for PlayStation 4 - Magma Red\n' +
                '                </div>\n' +
                '                <div class="s_product_options">\n' +
                '                    <label for="s_size">Size</label>\n' +
                '                    <select name="s_size" id="s_size">\n' +
                '                        <option value="M">M</option>\n' +
                '                        <option value="S">S</option>\n' +
                '                        <option value="L">L</option>\n' +
                '                        <option value="XL">XL</option>\n' +
                '                    </select>\n' +
                '                    <label for="s_color">Color</label>\n' +
                '                    <select name="s_color" id="s_color">\n' +
                '                        <option value="red">Red</option>\n' +
                '                        <option value="blue">Blue</option>\n' +
                '                        <option value="black">Black</option>\n' +
                '                        <option value="green">Green</option>\n' +
                '                    </select>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '            <div class="s_product_price">\n' +
                '                <span class="s_regular_price">250000&#8363;</span>\n' +
                '                <span class="s_old_price">300000&#8363;</span>\n' +
                '            </div>\n' +
                '            <div class="s_qty_product_counter">\n' +
                '                <span class="s_qty_minus s_qty_btn"></span>\n' +
                '                <span class="s_qty_value">125</span>\n' +
                '                <span class="s_qty_plus s_qty_btn"></span>\n' +
                '            </div>\n' +
                '            <button class="s_add_to_cart">Add to cart</button>\n' +
                '        </div>\n' +
                '        <div class="s_product_item">\n' +
                '            <div class="s_product_image">\n' +
                '                <img class="product-single__thumbnail-image"\n' +
                '                     src="//cdn.shopify.com/s/files/1/0405/1930/3334/products/image_png_758085347_110x110@2x.png?v=1602035422"\n' +
                '                     alt="Load image into Gallery viewer, Air Max 1 - luuthuy205">\n' +
                '            </div>\n' +
                '            <div class="s_product_detail">\n' +
                '                <div class="s_product_name">\n' +
                '                    DualShock 4 Wireless Controller for PlayStation 4 - Magma Red\n' +
                '                </div>\n' +
                '                <div class="s_product_options">\n' +
                '                    <label for="s_size">Size</label>\n' +
                '                    <select name="s_size" id="s_size">\n' +
                '                        <option value="M">M</option>\n' +
                '                        <option value="S">S</option>\n' +
                '                        <option value="L">L</option>\n' +
                '                        <option value="XL">XL</option>\n' +
                '                    </select>\n' +
                '                    <label for="s_color">Color</label>\n' +
                '                    <select name="s_color" id="s_color">\n' +
                '                        <option value="red">Red</option>\n' +
                '                        <option value="blue">Blue</option>\n' +
                '                        <option value="black">Black</option>\n' +
                '                        <option value="green">Green</option>\n' +
                '                    </select>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '            <div class="s_product_price">\n' +
                '                <span class="s_regular_price">250000&#8363;</span>\n' +
                '                <span class="s_old_price">300000&#8363;</span>\n' +
                '            </div>\n' +
                '            <div class="s_qty_product_counter">\n' +
                '                <span class="s_qty_minus s_qty_btn"></span>\n' +
                '                <span class="s_qty_value">125</span>\n' +
                '                <span class="s_qty_plus s_qty_btn"></span>\n' +
                '            </div>\n' +
                '            <button class="s_add_to_cart">Add to cart</button>\n' +
                '        </div>\n' +
                '        <div class="s_product_item">\n' +
                '            <div class="s_product_image">\n' +
                '                <img class="product-single__thumbnail-image"\n' +
                '                     src="//cdn.shopify.com/s/files/1/0405/1930/3334/products/image_png_758085347_110x110@2x.png?v=1602035422"\n' +
                '                     alt="Load image into Gallery viewer, Air Max 1 - luuthuy205">\n' +
                '            </div>\n' +
                '            <div class="s_product_detail">\n' +
                '                <div class="s_product_name">\n' +
                '                    DualShock 4 Wireless Controller for PlayStation 4 - Magma Red\n' +
                '                </div>\n' +
                '                <div class="s_product_options">\n' +
                '                    <label for="s_size">Size</label>\n' +
                '                    <select name="s_size" id="s_size">\n' +
                '                        <option value="M">M</option>\n' +
                '                        <option value="S">S</option>\n' +
                '                        <option value="L">L</option>\n' +
                '                        <option value="XL">XL</option>\n' +
                '                    </select>\n' +
                '                    <label for="s_color">Color</label>\n' +
                '                    <select name="s_color" id="s_color">\n' +
                '                        <option value="red">Red</option>\n' +
                '                        <option value="blue">Blue</option>\n' +
                '                        <option value="black">Black</option>\n' +
                '                        <option value="green">Green</option>\n' +
                '                    </select>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '            <div class="s_product_price">\n' +
                '                <span class="s_regular_price">250000&#8363;</span>\n' +
                '                <span class="s_old_price">300000&#8363;</span>\n' +
                '            </div>\n' +
                '            <div class="s_qty_product_counter">\n' +
                '                <span class="s_qty_minus s_qty_btn"></span>\n' +
                '                <span class="s_qty_value">125</span>\n' +
                '                <span class="s_qty_plus s_qty_btn"></span>\n' +
                '            </div>\n' +
                '            <button class="s_add_to_cart">Add to cart</button>\n' +
                '        </div>\n' +
                '        <div class="s_product_item">\n' +
                '            <div class="s_product_image">\n' +
                '                <img class="product-single__thumbnail-image"\n' +
                '                     src="//cdn.shopify.com/s/files/1/0405/1930/3334/products/image_png_758085347_110x110@2x.png?v=1602035422"\n' +
                '                     alt="Load image into Gallery viewer, Air Max 1 - luuthuy205">\n' +
                '            </div>\n' +
                '            <div class="s_product_detail">\n' +
                '                <div class="s_product_name">\n' +
                '                    DualShock 4 Wireless Controller for PlayStation 4 - Magma Red\n' +
                '                </div>\n' +
                '                <div class="s_product_options">\n' +
                '                    <label for="s_size">Size</label>\n' +
                '                    <select name="s_size" id="s_size">\n' +
                '                        <option value="M">M</option>\n' +
                '                        <option value="S">S</option>\n' +
                '                        <option value="L">L</option>\n' +
                '                        <option value="XL">XL</option>\n' +
                '                    </select>\n' +
                '                    <label for="s_color">Color</label>\n' +
                '                    <select name="s_color" id="s_color">\n' +
                '                        <option value="red">Red</option>\n' +
                '                        <option value="blue">Blue</option>\n' +
                '                        <option value="black">Black</option>\n' +
                '                        <option value="green">Green</option>\n' +
                '                    </select>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '            <div class="s_product_price">\n' +
                '                <span class="s_regular_price">250000&#8363;</span>\n' +
                '                <span class="s_old_price">300000&#8363;</span>\n' +
                '            </div>\n' +
                '            <div class="s_qty_product_counter">\n' +
                '                <span class="s_qty_minus s_qty_btn"></span>\n' +
                '                <span class="s_qty_value">125</span>\n' +
                '                <span class="s_qty_plus s_qty_btn"></span>\n' +
                '            </div>\n' +
                '            <button class="s_add_to_cart">Add to cart</button>\n' +
                '        </div>\n' +
                '        <div class="s_product_item">\n' +
                '            <div class="s_product_image">\n' +
                '                <img class="product-single__thumbnail-image"\n' +
                '                     src="//cdn.shopify.com/s/files/1/0405/1930/3334/products/image_png_758085347_110x110@2x.png?v=1602035422"\n' +
                '                     alt="Load image into Gallery viewer, Air Max 1 - luuthuy205">\n' +
                '            </div>\n' +
                '            <div class="s_product_detail">\n' +
                '                <div class="s_product_name">\n' +
                '                    DualShock 4 Wireless Controller for PlayStation 4 - Magma Red\n' +
                '                </div>\n' +
                '                <div class="s_product_options">\n' +
                '                    <label for="s_size">Size</label>\n' +
                '                    <select name="s_size" id="s_size">\n' +
                '                        <option value="M">M</option>\n' +
                '                        <option value="S">S</option>\n' +
                '                        <option value="L">L</option>\n' +
                '                        <option value="XL">XL</option>\n' +
                '                    </select>\n' +
                '                    <label for="s_color">Color</label>\n' +
                '                    <select name="s_color" id="s_color">\n' +
                '                        <option value="red">Red</option>\n' +
                '                        <option value="blue">Blue</option>\n' +
                '                        <option value="black">Black</option>\n' +
                '                        <option value="green">Green</option>\n' +
                '                    </select>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '            <div class="s_product_price">\n' +
                '                <span class="s_regular_price">250000&#8363;</span>\n' +
                '                <span class="s_old_price">300000&#8363;</span>\n' +
                '            </div>\n' +
                '            <div class="s_qty_product_counter">\n' +
                '                <span class="s_qty_minus s_qty_btn"></span>\n' +
                '                <span class="s_qty_value">125</span>\n' +
                '                <span class="s_qty_plus s_qty_btn"></span>\n' +
                '            </div>\n' +
                '            <button class="s_add_to_cart">Add to cart</button>\n' +
                '        </div>\n' +
                '        <div class="s_product_item">\n' +
                '            <div class="s_product_image">\n' +
                '                <img class="product-single__thumbnail-image"\n' +
                '                     src="//cdn.shopify.com/s/files/1/0405/1930/3334/products/image_png_758085347_110x110@2x.png?v=1602035422"\n' +
                '                     alt="Load image into Gallery viewer, Air Max 1 - luuthuy205">\n' +
                '            </div>\n' +
                '            <div class="s_product_detail">\n' +
                '                <div class="s_product_name">\n' +
                '                    DualShock 4 Wireless Controller for PlayStation 4 - Magma Red\n' +
                '                </div>\n' +
                '                <div class="s_product_options">\n' +
                '                    <label for="s_size">Size</label>\n' +
                '                    <select name="s_size" id="s_size">\n' +
                '                        <option value="M">M</option>\n' +
                '                        <option value="S">S</option>\n' +
                '                        <option value="L">L</option>\n' +
                '                        <option value="XL">XL</option>\n' +
                '                    </select>\n' +
                '                    <label for="s_color">Color</label>\n' +
                '                    <select name="s_color" id="s_color">\n' +
                '                        <option value="red">Red</option>\n' +
                '                        <option value="blue">Blue</option>\n' +
                '                        <option value="black">Black</option>\n' +
                '                        <option value="green">Green</option>\n' +
                '                    </select>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '            <div class="s_product_price">\n' +
                '                <span class="s_regular_price">250000&#8363;</span>\n' +
                '                <span class="s_old_price">300000&#8363;</span>\n' +
                '            </div>\n' +
                '            <div class="s_qty_product_counter">\n' +
                '                <span class="s_qty_minus s_qty_btn"></span>\n' +
                '                <span class="s_qty_value">125</span>\n' +
                '                <span class="s_qty_plus s_qty_btn"></span>\n' +
                '            </div>\n' +
                '            <button class="s_add_to_cart">Add to cart</button>\n' +
                '        </div>\n' +
                '        <div class="s_product_item">\n' +
                '        <div class="s_product_image">\n' +
                '            <img class="product-single__thumbnail-image"\n' +
                '                 src="//cdn.shopify.com/s/files/1/0405/1930/3334/products/image_png_758085347_110x110@2x.png?v=1602035422"\n' +
                '                 alt="Load image into Gallery viewer, Air Max 1 - luuthuy205">\n' +
                '        </div>\n' +
                '        <div class="s_product_detail">\n' +
                '            <div class="s_product_name">\n' +
                '                DualShock 4 Wireless Controller for PlayStation 4 - Magma Red\n' +
                '            </div>\n' +
                '            <div class="s_product_options">\n' +
                '                <label for="s_size">Size</label>\n' +
                '                <select name="s_size" id="s_size">\n' +
                '                    <option value="M">M</option>\n' +
                '                    <option value="S">S</option>\n' +
                '                    <option value="L">L</option>\n' +
                '                    <option value="XL">XL</option>\n' +
                '                </select>\n' +
                '                <label for="s_color">Color</label>\n' +
                '                <select name="s_color" id="s_color">\n' +
                '                    <option value="red">Red</option>\n' +
                '                    <option value="blue">Blue</option>\n' +
                '                    <option value="black">Black</option>\n' +
                '                    <option value="green">Green</option>\n' +
                '                </select>\n' +
                '            </div>\n' +
                '        </div>\n' +
                '        <div class="s_product_price">\n' +
                '            <span class="s_regular_price">250000&#8363;</span>\n' +
                '            <span class="s_old_price">300000&#8363;</span>\n' +
                '        </div>\n' +
                '        <div class="s_qty_product_counter">\n' +
                '            <span class="s_qty_minus s_qty_btn"></span>\n' +
                '            <span class="s_qty_value">125</span>\n' +
                '            <span class="s_qty_plus s_qty_btn"></span>\n' +
                '        </div>\n' +
                '        <button class="s_add_to_cart">Add to cart</button>\n' +
                '    </div>\n' +
                '    </div>\n' +
                '</div>';
            preview_html = preview_html.replace('var_s_1_title', data['title_bought_together']);
            $("#s_shopify_sticky_add_to_cart_setting_onboarding_iframe").contents().find("#s_shopify_sticky_add_to_cart_setting_onboarding_content").html(preview_html)
            $("#s_shopify_sticky_add_to_cart_setting_onboarding_iframe").contents().find("#s_shopify_sticky_add_to_cart_setting_onboarding_content_action_change").click()
        }
    }

    function re_draw_s_shopify_bought_together_setting_onboarding(data) {
            first_time_draw_iframe_interval = setInterval(function () {
                    var iframe = document.getElementById('s_shopify_sticky_add_to_cart_setting_onboarding_iframe');
                    if (iframe != undefined) {
                        var iframeDoc = iframe.contentDocument || iframe.contentWindow.document;
                        if (iframeDoc.readyState == 'complete') {
                            if ($("#s_shopify_sticky_add_to_cart_setting_onboarding_iframe").contents().find("#s_shopify_sticky_add_to_cart_setting_onboarding_content") != undefined) {
                                clearInterval(first_time_draw_iframe_interval);
                                update_generate_iframe_content(data)
                            }
                        }
                    }

                }, 100);
    }

    var OurFormController = FormController.extend({
        custom_events: _.extend({}, FormController.prototype.custom_events, {
            Controller: FormController,
            Renderer: FormRenderer,
        }),
        init: function (parent, model, renderer, params) {
            this._super.apply(this, arguments);
        },
        _confirmChange: function (id, fields, e) {
            re_draw_s_shopify_bought_together_setting_onboarding(this.model.get(e.target.dataPointID).data)

            return this._super.apply(this, arguments);
        },
        _update: function (state, params) {
            var result = this._super.apply(this, arguments)
            re_draw_s_shopify_bought_together_setting_onboarding(state.data)
            return result;
        },
    });

    var OurFormView = FormView.extend({
        config: _.extend({}, FormView.prototype.config, {
            Controller: OurFormController,
            Renderer: FormRenderer,
        }),
    });

    viewRegistry.add('s_shopify_sticky_add_to_cart_setting_onboarding_form', OurFormView);
    return OurFormView
});


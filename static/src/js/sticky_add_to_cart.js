//const BASE_URL = 'https://odoo.website/';
//const getUrl = (path = '') => {
//    return BASE_URL + path;
//}
var ConfigDetail = [];
function initCss() {
    var t = document.createElement("link");
    t.setAttribute("rel", "stylesheet"), t.setAttribute("type", "text/css"), t.setAttribute("href", "https://odoo.website/s_shopify_sticky_add_to_cart/static/src/css/stickybar.css"), document.head.appendChild(t)
}

function loadScript(url, callback) {

    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = url || '';

    if (script.readyState) {
        //IE
        script.onreadystatechange = function () {
            if (script.readyState == "loaded" || script.readyState == "complete") {
                script.onreadystatechange = null;
                callback();
            }
        };
    } else {
        //Others
        script.onload = function () {
            callback();
        };
    }
    document.getElementsByTagName("head")[0].appendChild(script);

};

function myAppJavaScript($) {


    // lay thong tin tu odoo
    function GetConfigStickyCart() {
        $.ajax({
            type: "POST",
            url: "https://odoo.website/s_shopify_sticky_add_to_cart/config_sticky_cart",
            dataType: 'json',
            data: JSON.stringify({jsonrpc: '2.0'}),
            contentType: "application/json",
            error: function (request, error) {
                console.log('error')
            },
            complete(data) {
                ConfigDetail = JSON.parse(data["responseText"])['result']
                console.log(ConfigDetail)
            },
        });
    }
    function GetConfigStickyBar() {
        $.ajax({
            type: "POST",
            url: "https://odoo.website/s_shopify_sticky_add_to_cart/config_sticky_bar",
            dataType: 'json',
            data: JSON.stringify({jsonrpc: '2.0'}),
            contentType: "application/json",
            error: function (request, error) {
                console.log('error')
            },
            complete(data) {
                ConfigDetail = JSON.parse(data["responseText"])['result']
                console.log(ConfigDetail)
            },
        });
    }
    function GetConfigMiniCart() {
        $.ajax({
            type: "POST",
            url: "https://odoo.website/s_shopify_sticky_add_to_cart/config_mini_cart",
            dataType: 'json',
            data: JSON.stringify({jsonrpc: '2.0'}),
            contentType: "application/json",
            error: function (request, error) {
                console.log('error')
            },
            complete(data) {
                ConfigDetail = JSON.parse(data["responseText"])['result']
                console.log(ConfigDetail)
            },
        });
    }
    GetConfigStickyBar()
    GetConfigStickyCart()
    GetConfigMiniCart()

}

// function view_shop_cart(ConfigDetail) {
//     console.log('vao roi')
//     var html = '' +
//         '<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">' +
//         '\n<div class="' + dataSettings[1].sticky_position + '" style="background-color:' + dataSettings[1].background_sticky_cart + '" id="' + dataSettings[1].button_action + '" onclick="openMiniCart(this.id)">' +
//         '<span class="mgn-minicart-icon ">' +
//         '<i class="' + dataSettings[1].choose_icon + '" aria-hidden="true" style="color:' + dataSettings[1].color_icon + '">' +
//         '</i></span><span class="mgn-minicart-count" style="background:' + dataSettings[1].color_counter + '"><p style="color:' + dataSettings[1].color_number + '">' + cartDetail.item_count + '</p></span>' +
//         '</div>'
//     var txt = $(html);
//     jQuery("body").append(txt);
// }

if (typeof jQuery === 'undefined' || parseFloat(jQuery.fn.jquery) < 1.7) {
    loadScript('//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js', function () {
        var jQuery191 = jQuery.noConflict(true);
        var link_css = '<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.css">\n' +
            '<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css">\n'
        jQuery191('head').append(link_css);
        myAppJavaScript(jQuery191);
        // view_shop_cart(ConfigDetail);
        set_sticky_bar();
        initCss();
    });
} else {
    myAppJavaScript(jQuery);
}
function set_sticky_bar(){
    var html = '<div class="mgn-preview-wrapper">\n' +
        '    <div class="mgn-preview-inner">\n' +
        '        <div class="mgn-stickybar">\n' +
        '            <div class="mgn-stickybar-content">\n' +
        '                <div class="mgn-product-photo">\n' +
        '                    <span class="mgn-product-image">\n' +
        '                        <img src="https://res.cloudinary.com/patch-gardens/image/upload/c_fill,f_auto,h_840,q_auto:good,w_840/v1563806764/products/snake-plant-cc2bbb.jpg" alt=""/>\n' +
        '                    </span>\n' +
        '                </div>\n' +
        '                <div class="mgn-product-details">\n' +
        '                    <p class="mgn-product-name"><a href="#">Snake Plant</a></p>\n' +
        '                    <p class="mgn-product-price"><span>$19.99</span></p>\n' +
        '                </div>\n' +
        '                <div class="mgn-product-options">\n' +
        '                    <div class="mgn-product-option mgn-product-option--size">\n' +
        '                        <select>\n' +
        '                            <option>Size (S) </option>\n' +
        '                            <option>Size (M) </option>\n' +
        '                            <option>Size (L) </option>\n' +
        '                        </select>\n' +
        '                    </div>\n' +
        '                    <div class="mgn-product-option mgn-product-option--qty">\n' +
        '                        <label>\n' +
        '                            <span class="mgn-option-label">Qty</span>\n' +
        '                            <input type="number">\n' +
        '                        </label>\n' +
        '                    </div>\n' +
        '                    <div class="mgn-product-option mgn-product-option--buynow">\n' +
        '                        <button class="mgn-buynow-btn">Buy now</button>\n' +
        '                    </div>\n' +
        '                    <div class="mgn-product-option mgn-product-option--addtocart">\n' +
        '                        <button class="mgn-addtocart-btn">Add to cart</button>\n' +
        '                    </div>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>';
    var txt = $(html);
    jQuery("body").append(txt);
    console.log('dat choat')
}

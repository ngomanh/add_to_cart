odoo.define('s_shopify_sticky_add_to_cart.widget_fields_icon', function (require) {
    "use strict";
    var AbstractField = require('web.AbstractField');
    var core = require('web.core');
    var registry = require('web.field_registry');
    var _t = core._t;
    var client = require('web.web_client');
    var UrlImage = AbstractField.extend({
        supportedFieldTypes: ['char'],
        _render: function () {
            this._super();
            var new_html_render = '<div class="choose_icon">\n' +
                '                                                <a><i\n' +
                '                                                    class="fas fa-shopping-cart" onclick="window.set_icon_cart()"></i></a>\n' +
                '                                                <a><i\n' +
                '                                                    class="fas fa-shopping-bag" onclick="window.set_icon_bag()"></i></a>\n' +
                '                                                <a><i class="fas fa-shopping-basket" onclick="window.set_icon_basket()"></i></a>\n' +
                '                                                <a><i class="fas fa-cart-plus" onclick="window.set_icon_cart_plus()"></i></a>\n' +
                '                                                <a><i class="fas fa-cart-arrow-down" onclick="window.set_icon_cart_arrow_down()"></i></a>\n' +
                '                                            </div>'
            this.$el.html(new_html_render);
        },
    });
    registry.add('widget_fields_icon', UrlImage);
    window.set_icon_cart = function(){
        client._rpc({
            model: 's.shopify.sticky.cart.config',
            method: 'set_icon_sticky_cart',
            args: [1],
            kwargs : {"icon": "fas fa-shopping-cart"}
        }).then(function (data) {
            console.log(data);
        });
    }
    window.set_icon_bag = function(){
        client._rpc({
            model: 's.shopify.sticky.cart.config',
            method: 'set_icon_sticky_cart',
            args: [1],
            kwargs : {"icon": "fas fa-shopping-bag"}
        }).then(function (data) {
            console.log(data);
        });
    }
    window.set_icon_basket = function(){
        client._rpc({
            model: 's.shopify.sticky.cart.config',
            method: 'set_icon_sticky_cart',
            args: [1],
            kwargs : {"icon": "fas fa-shopping-basket"}
        }).then(function (data) {
            console.log(data);
        });
    }
    window.set_icon_cart_plus = function(){
        client._rpc({
            model: 's.shopify.sticky.cart.config',
            method: 'set_icon_sticky_cart',
            args: [1],
            kwargs : {"icon": "fas fa-cart-plus"}
        }).then(function (data) {
            console.log(data);
        });
    }
    window.set_icon_cart_arrow_down = function(){
        client._rpc({
            model: 's.shopify.sticky.cart.config',
            method: 'set_icon_sticky_cart',
            args: [1],
            kwargs : {"icon": "fas fa-cart-arrow-down"}
        }).then(function (data) {
            console.log(data);
        });
    };
});
